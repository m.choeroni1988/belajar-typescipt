import { Request, Response} from "express";
import Authentication from "../utils/Authentication";
const db = require("../db/models");

class AuthController {
    register = async (req: Request, res: Response): Promise<Response> => {
        let { username, password } = req.body;
        const hashedPassword: string = await Authentication.passwordHash(password);

        const createUser = await db.user.create({ username, password: hashedPassword });

        return res.send("Registrasi sukses");
    }

    login = async (req: Request, res: Response): Promise<Response> => {
        // cari data user by username
        let { username, password } = req.body;

        const user = await db.user.findOne({
            where: { username }
        });

        // check password 
        if (user) {
            let compare = await Authentication.checkPassword(password, user.password);
            // return res.send(compare);

            if (compare) {
                let token = Authentication.generateToken(user.id, username, user.password);
                return res.send({ token });
            }

            return res.send("Password anda tidak sesuai.");
        }

        return res.send("Username tidak ditemukan.");

        // generate token
        
        return res.send(user);
    }

    profile = (req: Request, res: Response): Response => {
        return res.send(req.app.locals.credential);
    }
}

export default new AuthController();