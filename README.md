## Instalasi Awal

1. jalankan `npm init` nanti akan muncul file package.json
2. instalasi typescript `npm install typescript -D`
3. instalasi nodemon `npm install nodemon -D`
4. pada file package.json tambahkan scripts sebagai berikut :
    ```
    "scripts": {
        "tsc": "rm -rf build/ && tsc",
        "ts": "rm -rf build/ && tsc -w", untuk compile TS nya
        "dev": "nodemon ./build/index.js"
    }
    ```

5. buat folder `src` dan buat file didalamnya index.js
6. jalankan pada cms `./node_modules/.bin/tsc --init`
7. ubah settingnya sebagai berikut :

    ```
    "allowJs": true, 
    "outDir": "./build",
    ```
8. jalankan `npm install express`
9. jalankan `npm install @types/express -D`
10. jalankan `npm install body-parser`
11. jalankan `npm install morgan -D`
12. jalankan `npm install @types/morgan -D`
13. jalankan `npm install compression helmet cors`
14. jalankan `npm install @types/compression @types/helmet @types/cors -D`
15. jalankan `npm install dotenv`
16. jalankan `npm install sequelize sequelize-cli mysql2`
17. create file `.sequelizerc` lihat doc 
    https://sequelize.org/master/manual/migrations.html#the--code--sequelizerc--code--file
18. sesuaikan codingannya
    ```
    const path = require('path');
    require("dotenv").config();

    if (process.env.NODE_ENV == "development") {
        module.exports = {
            'config': path.resolve('src/config', 'database.js'),
            'models-path': path.resolve('src/db', 'models'),
            'seeders-path': path.resolve('src/db', 'seeders'),
            'migrations-path': path.resolve('src/db', 'migrations')
        };
    } else {
        module.exports = {
            'config': path.resolve('build/config', 'database.js'),
            'models-path': path.resolve('build/db', 'models'),
            'seeders-path': path.resolve('build/db', 'seeders'),
            'migrations-path': path.resolve('build/db', 'migrations')
        };
    }
    ```
19. jalankan init pada sequelize-cli `./node_modules/.bin/sequelize-cli init`
20. sesuaikan config/database.js nya
21. jalankan `npm install bcrypt`
22. jalankan `npm install @types/bcrypt -D`
23. jalankan `npm install express-validator`
24. jalankan `npm install jsonwebtoken`
25. jalankan `npm install @types/jsonwebtoken -D`


## Spesial Thanks to Channel Youtube Nusendra.com
https://youtube.com/playlist?list=PLnQvfeVegcJZHhImGvDpnp0P725Ykx4Qt

