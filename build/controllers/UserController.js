"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let data = [
    { id: 1, name: "Adi" },
    { id: 2, name: "Budi" },
    { id: 3, name: "Cinta" },
    { id: 4, name: "Fanny" }
];
class UserController {
    index(req, res) {
        return res.send(data);
    }
    create(req, res) {
        const { id, name } = req.body;
        data.push({ id, name });
        return res.send('Create data sukses');
    }
    show(req, res) {
        const { id } = req.params;
        let person = data.find(item => item.id == id);
        return res.send(person);
    }
    update(req, res) {
        const { id } = req.params;
        const { name } = req.body;
        // cari data dari array
        let person = data.find(item => item.id == id);
        person.name = name;
        return res.send("Update data sukses");
    }
    delete(req, res) {
        const { id } = req.params;
        // filter data dari array selain id ini
        let people = data.filter(item => item.id != id);
        return res.send(people);
    }
}
exports.default = new UserController();
